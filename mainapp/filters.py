import django_filters
from django_filters import *
from .models import *

class KategoriFilter(django_filters.FilterSet):
	
	# nama = CharFilter(field_name='nama', lookup_expr='icontains')


	class Meta:
		model = Barang
		fields = ['kategori']
		